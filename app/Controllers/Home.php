<?php

namespace App\Controllers;

use App\ThirdParty\Pdf;

class Home extends BaseController {

    public function index(): string {
        return view('welcome_message');
    }

    public function jaime(): string {
        $data['title'] = 'Inicio';
        return view('inicio', $data);
    }

    public function tabla() {
        $data['title'] = 'Esto es una ejemplo de uso de datatables';
        return view('prueba/tabla', $data);
    }
     public function contacto() {
        $data['title'] = 'Contacto';
        return view('contacto', $data);
    }
    
    
    public function pdf() {
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 050');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
// set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 050',
                PDF_HEADER_STRING);
// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
// ---------------------------------------------------------
// NOTE: 2D barcode algorithms must be implemented on 2dbarcode.php class file.
// set font
        $pdf->SetFont('helvetica', '', 11);
// add a page
//$pdf->AddPage();
// print a message
        $txt = "You can also export 2D barcodes in other formats (PNG, SVG, HTML). Check the
examples inside the barcode directory.\n";
        $pdf->MultiCell(70, 50, $txt, 0, 'J', false, 1, 125, 30, true, 0, false, true, 0, 'T',
                false);
        $pdf->SetFont('helvetica', '', 10);
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// set style for barcode
        $style = array(
            'border' => true,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );
// write RAW 2D Barcode
        $code = '111011101110111,010010001000010,010011001110010,010010000010010,010011101110010';
        $pdf->write2DBarcode($code, 'RAW', 80, 30, 30, 20, $style, 'N');
// write RAW2 2D Barcode
        $code = '[111011101110111][010010001000010][010011001110010][010010000010010][010011101110010]';
        $pdf->write2DBarcode($code, 'RAW2', 80, 60, 30, 20, $style, 'N');
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// set style for barcode
        $style = array(
            'border' => 2,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );
// QRCODE,L : QR-CODE Low error correction
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'QRCODE,L', 20, 30, 50, 50, $style,
                'N');
        $pdf->Text(20, 25, 'QRCODE L');
// QRCODE,M : QR-CODE Medium error correction
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'QRCODE,M', 20, 90, 50, 50, $style,
                'N');
        $pdf->Text(20, 85, 'QRCODE M');
// QRCODE,Q : QR-CODE Better error correction
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'QRCODE,Q', 20, 150, 50, 50,
                $style, 'N');
        $pdf->Text(20, 145, 'QRCODE Q');
// QRCODE,H : QR-CODE Best error correction
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'QRCODE,H', 20, 210, 50, 50,
                $style, 'N');
        $pdf->Text(20, 205, 'QRCODE H');
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'PDF417', 80, 90, 0, 30, $style,
                'N');
        $pdf->Text(80, 85, 'PDF417 (ISO/IEC 15438:2006)');
// -------------------------------------------------------------------
// DATAMATRIX (ISO/IEC 16022:2006)
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'DATAMATRIX', 80, 150, 50, 50,
                $style, 'N');
        $pdf->Text(80, 145, 'DATAMATRIX (ISO/IEC 16022:2006)');
// -------------------------------------------------------------------
// new style
        $style = array(
            'border' => 2,
            'padding' => 'auto',
            'fgcolor' => array(0, 0, 255),
            'bgcolor' => array(255, 255, 64)
        );
// QRCODE,H : QR-CODE Best error correction
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'QRCODE,H', 80, 210, 50, 50,
                $style, 'N');
        $pdf->Text(80, 205, 'QRCODE H - COLORED');
// new style
        $style = array(
            'border' => false,
            'padding' => 0,
            'fgcolor' => array(128, 0, 0),
            'bgcolor' => false
        );
// QRCODE,H : QR-CODE Best error correction
        $pdf->write2DBarcode('https://www.ausiasmarch.net', 'QRCODE,H', 140, 210, 50, 50,
                $style, 'N');
        $pdf->Text(140, 205, 'QRCODE H - NO PADDING');
// ---------------------------------------------------------
//Close and output PDF document
        $pdf->Output('example_050.pdf', 'D');
    }

}
