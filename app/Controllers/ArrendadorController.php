<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\ArrendadorModel;

class ArrendadorController extends BaseController {

// controlador que mostrará todos los arrendadores
    public function index() {
        $arrendadores = new ArrendadorModel(); // * Se utiliza use arriba para evitar ponerlo aquí, 

        $data['title'] = 'Arrendador'; //Lo llama en la vista en un h2
        $data['arrendadores'] = $arrendadores->findAll();
        return view('arrendador/arrendadortabla', $data);
        //var_dump($data['arrendadores']);
    }

}
