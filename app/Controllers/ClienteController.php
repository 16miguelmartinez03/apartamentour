<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ClienteModel; 

class ClienteController extends BaseController {
// controlador que mostrará todos los clientes
    public function index(){
	$clientes = new ClienteModel(); // * Se utiliza use arriba para evitar ponerlo aquí, 
        //es más “limpio”
	//echo ‘<pre>’; → No hacen falta porque vamos a hacer una vista
		$data['title'] = 'Cliente'; //Lo llama en la vista en un h2
	$data['clientes'] = $clientes->findAll(); 
        //// print_r($clientes->find(4)); -> busca/encuentra el id=
return view('cliente/clientetabla',$data);
        //var_dump($data['clientes']);
    }
}

