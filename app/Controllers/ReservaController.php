<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ReservaModel; 

class ReservaController extends BaseController {
    public function index(){
	$reservas = new ReservaModel(); // * Se utiliza use arriba para evitar ponerlo aquí, 
		$data['title'] = 'Reserva'; //Lo llama en la vista en un h2
	$data['reservas'] = $reservas->findAll(); 

return view('reserva/reservatabla',$data);
    }
}
