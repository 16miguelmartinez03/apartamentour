<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\ApartamentoModel;

class ApartamentoController extends BaseController {

    public function index() {
        $apartamentos = new ApartamentoModel(); // * Se utiliza use arriba para evitar ponerlo aquí, 
        $data['title'] = 'Apartamento'; //Lo llama en la vista en un h2
        $data['apartamentos'] = $apartamentos->findAll();
        return view('apartamento/apartamentotabla', $data);
    }
    
         public function apartamento() {
        $data['title'] = 'Apartamentos';
        return view('apartamentos', $data);
    }
     public function apartamentos() {
        $data['title'] = 'Apartamentos';
        return view('apartamento/apartamentos', $data);
    }
     public function grove() {
        $data['title'] = 'Apartamentos';
        return view('apartamento/grove', $data);
    }
     public function bajob() {
        $data['title'] = 'Apartamentos';
        return view('apartamento/bajob', $data);
    }
     public function walter() {
        $data['title'] = 'Apartamentos';
        return view('apartamento/walter', $data);
    }

}
