<?php
namespace App\ThirdParty;
use TCPDF;
class Pdf extends TCPDF {
	function __construct()
	{
	parent::__construct();
	$this->header();
	$this->footer();
	}

	//Page header
	public function Header() {
	// Logo
	$image_file = ROOTPATH.'public/assets/images/logoausias.png';
	$this->Image($image_file, 20, 10, 10, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	// Set font
	$this->SetFont('helvetica', 'B', 12);
	// Title
	$this->Cell(0, 15, "<< TCPDF Example 003 >>", 0, false, 'C', 0, '', 0, false, 'M','M');
	}

	// Page footer
	public function Footer() {
	// Position at 15 mm from bottom
	$this->SetY(-15);
	// Set font
	$this->SetFont('helvetica', 'I', 8);
	// Page number
	$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0,false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
