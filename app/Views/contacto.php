<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>
<div class="container" style="max-width: 800px; margin: 0 auto; padding: 20px;">
  <h1 style="color: #333;">Contacto</h1>
  <p style="color: #666; font-size: 18px;">Por favor, completa el siguiente formulario para ponerte en contacto con nosotros.</p>
  <div class="contact-form" style="background-color: #fff; padding: 20px; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);">
    <form action="#" method="post">
      <div class="form-group" style="margin-bottom: 20px;">
        <label for="nombre" style="display: block; font-weight: bold; margin-bottom: 5px;">Nombre:</label>
        <input type="text" id="nombre" name="nombre" placeholder="Ausiàs March" style="width: 100%; padding: 10px; border: 1px solid #ccc; border-radius: 5px; font-size: 16px;">
      </div>
      <div class="form-group" style="margin-bottom: 20px;">
        <label for="email" style="display: block; font-weight: bold; margin-bottom: 5px;">Correo Electrónico:</label>
        <input type="email" id="email" name="email" placeholder="ausias@ausiasmarch.es" style="width: 100%; padding: 10px; border: 1px solid #ccc; border-radius: 5px; font-size: 16px;">
      </div>
      <div class="form-group" style="margin-bottom: 20px;">
        <label for="mensaje" style="display: block; font-weight: bold; margin-bottom: 5px;">Mensaje:</label>
        <textarea id="mensaje" name="mensaje" placeholder="En realidad no podremos recibir tu mensaje, pero gracias por tu interés" style="width: 100%; height: 100px; padding: 10px; border: 1px solid #ccc; border-radius: 5px; font-size: 16px;"></textarea>
      </div>
      <div class="form-group" style="margin-bottom: 20px;">
        <input type="submit" value="Enviar" style="background-color: #333; color: #fff; border: none; padding: 10px 20px; border-radius: 5px; cursor: pointer; font-size: 16px;">
      </div>
    </form>
  </div>
</div>
<?= $this->endSection() ?>

