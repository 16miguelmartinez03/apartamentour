<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<h1><?= $title ?></h1>
<?= $this->endSection() ?>

<?= $this->section('content')?>
<table id="myTable" class="table table-stripped table-bordered">
    <thead>
        <tr>
            <th>CodArr</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Direccion</th>￼
            <th>Correo</th>
            <th>Telefono</th>
            <th>FechaNac</th>￼
        </tr>
    </thead>
    <tbody>
        <?php foreach ($arrendadores as $arrendador): ?> <!--RECORDAD : en html es el equivalente a  { -->
            <tr>
                <td><?= $arrendador->CodArr ?></td>
                <td><?= $arrendador->Nombre ?> </td>
                <td><?= $arrendador->Apellido ?></td>
                <td><?= $arrendador->Direccion ?></td>
                <td><?= $arrendador->Correo ?></td>
                <td><?= $arrendador->Telefono ?></td>
                <td><?= $arrendador->FechaNac ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?= $this->endSection() ?>

