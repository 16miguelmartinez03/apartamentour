<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>



    <!-- Carousel con las imágenes -->
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?= base_url('assets/images/bajob.jpg')?>" class="d-block w-100" alt="Los Angeles" height="500px">
      </div>
      <div class="carousel-item">
        <img src="<?= base_url('assets/images/coke.jpg')?>" class="d-block w-100" alt="Chicago" height="500px">
      </div>
      <div class="carousel-item">
          <img src="<?= base_url('assets/images/comunidad.jpg')?>" class="d-block w-100" alt="New York" height="500px">
      </div>
        <div class="carousel-item">
          <img src="<?= base_url('assets/images/montepinar1-a.jpg')?>" class="d-block w-100" alt="New York" height="500px">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

    <!-- Información -->
<h1>Bajo B</h1>
<h2>Mirador de Montepinar</h2>
<p>Impresionante residencia ubicada en Mirador de Montepinar, destacando por su arquitectura moderna y espacios generosos. Con una fachada elegante y cuidadosamente decorada, ofrece interiores lujosos con muebles de alta calidad y detalles contemporáneos. Los amplios espacios comunes y las habitaciones bien equipadas brindan comodidad y estilo, mientras que el patio o jardín exterior proporciona un lugar ideal para el entretenimiento y la relajación al aire libre.
</p>

<h1><span class="text-success">100€</span>/noche/persona</h1>

<!-- Agrega los scripts de Bootstrap -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<?= $this->endSection() ?>