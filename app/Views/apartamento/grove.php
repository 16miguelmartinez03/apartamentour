<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>



    <!-- Carousel con las imágenes -->
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?= base_url('assets/images/grove.jpg')?>" class="d-block w-100" alt="Los Angeles" height="500px">
      </div>
      <div class="carousel-item">
        <img src="<?= base_url('assets/images/grove1.jpg')?>" class="d-block w-100" alt="Chicago" height="500px">
      </div>
      <div class="carousel-item">
          <img src="<?= base_url('assets/images/grove2.jpg')?>" class="d-block w-100" alt="New York" height="500px">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

    <!-- Información -->
<h1>Grove St. San Andreas</h1>
<h2>Complejo Familiar</h2>
<p>Grove Street está situada en el distrito de Ganton, y se caracteriza por sus casas de una sola planta y su apariencia urbana modesta. Es un vecindario que refleja la vida de clase trabajadora y la cultura de pandillas. Las calles están llenas de graffiti, y la vida en Grove Street está marcada por la violencia de las pandillas y los problemas socioeconómicos.</p>

<h1><span class="text-success">50€</span>/noche/persona</h1>

  <!-- Enlace al video -->
        <div class="text-center mt-4">
          <a href="https://www.youtube.com/watch?v=sOvyNa9-39c" class="btn btn-primary" target="_blank">Ver video del apartamento</a>
        </div>


<!-- Agrega los scripts de Bootstrap -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<?= $this->endSection() ?>