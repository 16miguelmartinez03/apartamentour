<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<h1><?= $title ?></h1>
<?= $this->endSection() ?>

<?= $this->section('content')?>
<table id="myTable" class="table table-stripped table-bordered">
    <thead>
        <tr>
            <th>CodApart</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Arrendador_id</th>￼
            <th>PrecioNoche</th>
            <th>Disponibilidad</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($apartamentos as $apartamento): ?> <!--RECORDAD : en html es el equivalente a  { -->
            <tr>
                <td><?= $apartamento->CodApart ?></td>
                <td><?= $apartamento->Nombre ?> </td>
                <td><?= $apartamento->Descripcion ?></td>
                <td><?= $apartamento->Arrendador_id ?></td>
                <td><?= $apartamento->PrecioNoche ?></td>
                <td><?= $apartamento->Disponibilidad ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?= $this->endSection() ?>

