<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>
<h3>En esta página verás los distintos apartamentos que tenemos disponibles en nuestra página.</h3>
<div class="container">
  <div class="row">
    <div class="col-lg-4 col-md-6 mb-4">
      <div class="card">
        <img src="<?= base_url('assets/images/grove.jpg')?>" class="img-square" alt="User Image" width="365px" height="250px">
        <div class="card-body">
          <h5 class="card-title">Grove St. San Andreas</h5>
          <p class="card-text">(COMPLEJO FAMILIAR) Grove Street está situada en el distrito de Ganton, y se caracteriza por sus casas de una sola planta y su apariencia urbana modesta. Es un vecindario que refleja la vida de clase trabajadora y la cultura de pandillas. Las calles están llenas de graffiti, y la vida en Grove Street está marcada por la violencia de las pandillas y los problemas socioeconómicos.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 mb-4">
      <div class="card">
        <img src="<?= base_url('assets/images/bajob.jpg')?>" class="img-square" alt="User Image" width=365px" height="250px">
        <div class="card-body">
          <h5 class="card-title">Bajo B, Mirador de Montepinar</h5>
          <p class="card-text">Impresionante residencia ubicada en Mirador de Montepinar, destacando por su arquitectura moderna y espacios generosos. Con una fachada elegante y cuidadosamente decorada, ofrece interiores lujosos con muebles de alta calidad y detalles contemporáneos. Los amplios espacios comunes y las habitaciones bien equipadas brindan comodidad y estilo, mientras que el patio o jardín exterior proporciona un lugar ideal para el entretenimiento y la relajación al aire libre.</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 mb-4">
      <div class="card">
        <img src="<?= base_url('assets/images/walter.jpg')?>" class="img-square" alt="User Image" width="365px" height="250px">
        <div class="card-body">
          <h5 class="card-title">Casa Walter White</h5>
          <p class="card-text">Modesta residencia ubicada en Albuquerque, Nuevo México. Situada en un vecindario suburbano, la casa se presenta como una estructura de una sola planta con un diseño típico de mediados de siglo. Su exterior está pintado en tonos neutros, con un garaje adjunto y un pequeño jardín delantero. Aunque aparentemente común desde afuera, el interior revela una decoración simple pero ordenada, con muebles funcionales y toques personales discretos. Pizza incluida</p>
        </div>
      </div>
    </div>
  </div>
</div>

<?= $this->endSection() ?>

