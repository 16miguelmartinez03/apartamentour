<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<h1><?= $title ?></h1>
<?= $this->endSection() ?>

<?= $this->section('content')?>
<table id="myTable" class="table table-stripped table-bordered">
        	<thead>
            	<tr>
                	<th>CodCli</th>
                	<th>Nombre</th>
                        <th>Apellido</th>
                	<th>Direccion</th>
                	<th>Correo</th>
                        <th>Telefono</th>
            	</tr>
        	</thead>
        	<tbody>
            	<?php foreach($clientes as $cliente): ?> <!--RECORDAD : en html es el equivalente a  { -->
            	<tr>
                	<td><?= $cliente-> CodCli ?></td>
                	<td><?= $cliente->Nombre ?> </td>
                        <td><?= $cliente->Apellido ?></td>
                	<td><?= $cliente->Direccion ?></td>
                	<td><?= $cliente->Correo ?></td>
                        <td><?= $cliente->Telefono ?></td>
            	</tr>
            	<?php endforeach; ?>
        	</tbody>
    	</table>

<?= $this->endSection() ?>

