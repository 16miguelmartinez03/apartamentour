<?= $this->section('css') ?>
<link href="assets/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<?= $this->endSection() ?>
<?= $this->section('js') ?>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function () {
$('#myTable').DataTable();
});
</script>
<?= $this->endSection() ?>