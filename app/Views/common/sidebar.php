<!-- Sidebar -->

<a href="../../index3.html" class="brand-link">
    <img src="<?= base_url('assets/images/logoausias.png') ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">ApartamenTour</span>
</a>

<div class="sidebar">

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="<?= base_url('assets/images/goku.png') ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <?php if (auth()->loggedIn()): ?>
                <a href="<?= base_url('logout') ?>" class="d-block">
                    <span style="font-family: monospace; font-size: 14px; color: #6DB33F; padding: 2px 4px;" >
                        <?= auth()->user()->username ?>
                        <?= auth()->user()->apellido1 ?>
                        <?= auth()->user()->apellido2 ?>
                    </span>
                    <span style="font-style: italic; color: #FF5733;">&nbsp&nbsp(salir)</span>
                </a>
            <?php else: ?>
                <a href="login" class="d-block"> Autenticar</a>
            <?php endif ?>
        </div>
    </div>

    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Buscar" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div><div class="sidebar-search-results"><div class="list-group"><a href="#" class="list-group-item"><div class="search-title"><strong class="text-light"></strong>N<strong class="text-light"></strong>o<strong class="text-light"></strong> <strong class="text-light"></strong>e<strong class="text-light"></strong>l<strong class="text-light"></strong>e<strong class="text-light"></strong>m<strong class="text-light"></strong>e<strong class="text-light"></strong>n<strong class="text-light"></strong>t<strong class="text-light"></strong> <strong class="text-light"></strong>f<strong class="text-light"></strong>o<strong class="text-light"></strong>u<strong class="text-light"></strong>n<strong class="text-light"></strong>d<strong class="text-light"></strong>!<strong class="text-light"></strong></div><div class="search-path"></div></a></div></div>
    </div>

    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header"> CLIENTES </li>
            <li class="nav-item">
                <a href="<?php base_url('../reservar.php') ?>" class="nav-link">
                    <i class="nav-icon fas fa-bookmark"></i>
                    <p>
                        Reservar
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php base_url('../factura.php') ?> " class="nav-link">
                    <i class="nav-icon far fa-solid fa-file-invoice"></i>
                    <p>
                        Factura
                    </p>
                </a>
            </li>
            <li class="nav-header">ARRENDADORES  </li>
            <li class="nav-item">
                <a href="../ingresos.html" class="nav-link" target="_blank">
                    <i class="nav-icon far fa-solid fa-euro-sign"></i>
                    <p> Resumen Ingresos </p>
                </a>
            </li>
            <li class="nav-header"> GENERAL </li>
            <li class="nav-item">
                <a href="<?= base_url('apartamento/apartamentos') ?>" class="nav-link">
                    <i class="nav-icon far fa-image"></i>
                    <p>
                        Imágenes
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="<?= base_url('apartamentos') ?>" class="nav-link">
                    <i class="fa-solid fa-database"></i>
                    <p>&nbsp Apartamentos </p>
                    <i class="fas fa-angle-left right"></i>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="<?= base_url('apartamento/grove') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-house nav-icon"></i>
                            <p>Apartamento 1</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('apartamento/bajob') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-building nav-icon"></i>
                            <p>Apartamento 2</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('/apartamento/walter') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-house nav-icon"></i>
                            <p>Apartamento 3</p>
                        </a>
                    </li>
                </ul>
            <li class="nav-header"> INTRANET </li>
            <a href="<?= base_url('auth/user') ?>" class="nav-link" target="_blank">
                <i class="far fa-solid fa-shield-halved" ></i>
                <p>&nbsp Grupos</p>
            </a>
            </li>

            <li class="nav-item">
                <a href="<?= base_url('apartamentos') ?>" class="nav-link">
                    <i class="fa-solid fa-database"></i>
                    <p>&nbsp Gestión </p>
                    <i class="fas fa-angle-left right"></i>
                </a>

                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="<?= base_url('cliente/tabla') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-bag-shopping nav-icon"></i>
                            <p>Clientes</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('arrendador/tabla') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-sack-dollar nav-icon"></i>
                            <p>Arrendadores</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('/apartamento/tabla') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-bed nav-icon"></i>
                            <p>Apartamentos</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('reserva/tabla') ?>" class="nav-link" target="_blank">
                            <i class="fa-solid fa-address-book nav-icon"></i>
                            <p>Reservas</p>
                        </a>
                    </li>






                    </nav>
                    </div>



