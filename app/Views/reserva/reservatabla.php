<?php
/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 * **************************************************************************** */
?>

<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<h1><?= $title ?></h1>
<?= $this->endSection() ?>

<?= $this->section('content')?>
<table id="myTable" class="table table-stripped table-bordered">
    <thead>
        <tr>
            <th>CodRes</th>
            <th>CodCli</th>
            <th>CodApart</th>
            <th>Fecha_in</th>￼
            <th>Fecha_out</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($reservas as $reserva): ?> <!--RECORDAD : en html es el equivalente a  { -->
            <tr>
                <td><?= $reserva->CodRes ?></td>
                <td><?= $reserva->CodCli ?> </td>
                <td><?= $reserva->CodApart ?></td>
                <td><?= $reserva->FechaIn ?></td>
                <td><?= $reserva->FechaOut ?></td>
                <td><?= $reserva->Estado ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?= $this->endSection() ?>

