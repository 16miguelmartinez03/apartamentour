<?php

/* * *****************************************************************************
 * Ejemplo de vista que utiliza la plantilla de adminlte y datatables
 *
 * **************************************************************************** */
?>
<?= $this->extend('plantillas/adminlte') ?>

<?= $this->section('css') ?>
<?= $this->include('common/datatables_css') ?>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?= $this->include('common/datatables_js') ?>
<?= $this->endSection() ?>

<?= $this->section('page_title') ?>
<?= $title ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<table id="myTable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Row 1 Data 1</td>
            <td>Row 1 Data 2</td>
        </tr>
        <tr>
            <td>Row 2 Data 1</td>
            <td>Row 2 Data 2</td>
        </tr>
        <tr>
            <td>Row 3 Data 1</td>
            <td>Row 3 Data 2</td>
        </tr>
    </tbody>
</table>
<?= $this->endSection() ?>
