<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of ClienteModel
 *
 * @author a003161627r
 */
namespace App\Models;
use CodeIgniter\Model;

// Esta clase conecta con la tabla cliente de nuestra base de datos y hace búsqueda de los campos
// Esta informacion será vista todo el mundo, es una lista de los apartamentos
class ApartamentoModel extends Model{
	protected $table	= 'apartamento';
	protected $primaryKey = 'CodApart';
	protected $useAutoIncrement = true; 
	protected $returnType		= 'object';  // array/object -> No cambia exactamente(estructura de variable distinta), él prefiere object
	protected $allowedFields = ['Nombre', 'Descripcion', 'Arrendador_id', 'PrecioNoche','Disponibilidad']; // Se supone que sin esto también funciona
}
