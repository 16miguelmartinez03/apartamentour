<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of ClienteModel
 *
 * @author a003161627r
 */
namespace App\Models;
use CodeIgniter\Model;

// Esta clase conecta con la tabla cliente de nuestra base de datos y hace búsqueda de los campos
// CodCli, Nombre, Apellido, Direccion, Correo y Telefono
// Esta informacion será vista únicamente por usuarios administradores, no cualquier
// usuario logeado será capaz de verla
class ClienteModel extends Model{
	protected $table	= 'cliente';
	protected $primaryKey = 'CodCli';
	protected $useAutoIncrement = true; 
	protected $returnType		= 'object';  // array/object -> No cambia exactamente(estructura de variable distinta), él prefiere object
	protected $allowedFields = ['Nombre', 'Apellido', 'Direccion', 'Correo','Telefono']; // Se supone que sin esto también funciona
}

