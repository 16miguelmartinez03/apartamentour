<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::jaime');
$routes->get('/loco', 'LoController::loco');
//$routes->get('/jaime', 'Home::jaime');
$routes->get('/tabla', 'Home::tabla');
$routes->get('/contacto', 'Home::contacto');


$routes->get('/apartamentos', 'ApartamentoController::apartamento');
$routes->get('apartamento/apartamentos', 'ApartamentoController::apartamentos');
$routes->get('apartamento/grove', 'ApartamentoController::grove');
$routes->get('apartamento/bajob', 'ApartamentoController::bajob');
$routes->get('apartamento/walter', 'ApartamentoController::walter');


$routes->get('/cliente/tabla', 'ClienteController::index');
$routes->get('/arrendador/tabla', 'ArrendadorController::index');
$routes->get('/apartamento/tabla', 'ApartamentoController::index');
$routes->get('/reserva/tabla', 'ReservaController::index');

$routes->get('/pdf', 'Home::pdf');

service('auth')->routes($routes);
