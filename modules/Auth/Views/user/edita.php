<?= $this->extend('Modules\Auth\Views\templates\default') ?>

<?= $this->section('content') ?>
    <div class="container d-flex justify-content-center p-5">
        <div class="card col-12 col-md-5 shadow-sm">
            <div class="card-body">
                <h5 class="card-title mb-5"><?= $titulo ?></h5>

                <?php if (session('error') !== null) : ?>
                    <div class="alert alert-danger" role="alert"><?= session('error') ?></div>
                <?php elseif (session('errors') !== null) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?php if (is_array(session('errors'))) : ?>
                            <?php foreach (session('errors') as $error) : ?>
                                <?= $error ?>
                                <br>
                            <?php endforeach ?>
                        <?php else : ?>
                            <?= session('errors') ?>
                        <?php endif ?>
                    </div>
                <?php endif ?>

                <?= form_open(site_url('auth/user/save/'.$user->id))?>    
                    <?= csrf_field() ?>

                    <!-- First Name -->
                    <div class="form-floating mb-2">
                        <?= form_label('Nombre: ','floatingFirstnameInput') ?>
                        <?= form_input('firstname',set_value('firstname',$user->firstname),['id'=>'floatingFirstnameInput','class'=>"form-control",'required'=>'required'])?>
                    </div>
                    
                    <!-- Last Name -->
                    <div class="form-floating mb-2">
                        <?= form_label('Apellidos: ','floatingLastnameInput') ?>
                        <?= form_input('lastname',set_value('lastname',$user->lastname),['id'=>'floatingLastnameInput','class'=>"form-control",'required'=>'required'])?>
                    </div>
                    
                    <!-- Email -->
                    <div class="form-floating mb-2">
                        <?= form_label('E-mail: ','floatingEmailInput') ?>
                        <?= form_input('email',set_value('email', $user->email),['id'=>'floatingEmailInput','class'=>"form-control",'required'=>'required'])?>
                    </div>

                    <!-- Username -->
                    <div class="form-floating mb-2">
                        <?= form_label('Username: ','floatingUsernameInput') ?>
                        <?= form_input('username',set_value('username', $user->username),['id'=>'floatingUsernameInput','class'=>"form-control",'required'=>'required'])?>
                    </div>

                    <!-- Password -->
                    <div class="form-floating mb-2">
                        <?= form_label('Contraseña','floatingPasswordInput') ?>
                        <?= form_password('password','',['id'=>'floatingPasswordInput','class'=>"form-control",'required'=>'required'])?>
                    </div>

                    <!-- Password (Again) -->
                    <div class="form-floating mb-2">
                        <?= form_label('Contraseña (otra vez)','floatingPasswordConfirmInput') ?>
                        <?= form_password('password_confirm','',['id'=>'floatingPasswordConfirmInput','class'=>"form-control",'required'=>'required'])?>
                    </div>

                    <div class="d-grid col-12 col-md-8 mx-auto m-3">
                        <?= form_submit('submit','Guardar',['class' => 'btn btn-primary btn-block'])?>
                    </div>

                    <p class="text-center"><?= lang('Auth.haveAccount') ?> <a href="<?= url_to('login') ?>"><?= lang('Auth.login') ?></a></p>

                <?= form_close()?>
            </div>
        </div>
    </div>

<?= $this->endSection() ?>
